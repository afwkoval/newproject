const area = document.querySelector('#area');
const city = document.querySelector('#city');

function turnDate(uniTime) {
  const myDate = new Date(uniTime * 1000);
  return myDate.toGMTString();
}

function turnToCels(temp) {
  return (temp - 273.15).toFixed(1);
}

async function set5DaysWeatherForecast(lat, lng) {
  const response = await fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lng}&exclude=alerts,&appid=c2a4d3c2abd98c7796b91be50e976075`);

  if (response.ok) {
    const forecast = await response.json();

    for (let i = 0; i < 5; i++) {
      const { temp, dt } = forecast.daily[i];
      const { min, max } = temp;
      const readyForecast = turnDate(dt) + ': ' + turnToCels(min) + '-' + turnToCels(max) + '°C';
      document.getElementById(`weath${i}`).innerHTML = readyForecast;
    }
  } else {
    alert('error', response.status);
  }
}


async function getData() {
  const response = await fetch('https://gist.githubusercontent.com/alex-oleshkevich/6946d85bf075a6049027306538629794/raw/3986e8e1ade2d4e1186f8fee719960de32ac6955/by-cities.json');
  
  if (response.ok) {
    const data = await response.json();
    const regions = {}
    data[0].regions.forEach(({ name, cities }) => regions[name] = cities)
    return regions;
  } else {
    alert('error', response.status);
  }
}

async function displayWeather () {
  const regions = await getData();

  const regionName = area.value;
  const cityName = city.value;

  const { lat, lng } = regions[regionName].find(city => city.name === cityName)
  await set5DaysWeatherForecast(lat, lng);
}

async function getWeatherByCity(initialCity) {
  if (initialCity) {
    displayWeather()
  }

  city.addEventListener('click', displayWeather);
}

async function start() {
    const regions = await getData();
    
    area.innerHTML = Object.keys(regions).map(regionName => `<option value="${regionName}">${regionName}</option>`).join('');

    area.addEventListener('change', function () {
      city.hidden = false;
      city.innerHTML = regions[this.value]
        .map((city) => `<option value="${city.name}">${city.name}</option>`)
        .join('');

        getWeatherByCity(city.value);
    });
}
  

start();